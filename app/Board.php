<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    protected $table = 'mkb_keijiban';
    protected $primaryKey = 'b_id';

    //protected $guarded = ['b_closed'];

    //public $timestamps = false;

    const CREATED_AT = 'b_creation_date';
    const UPDATED_AT = 'b_last_update';

    protected $attributes = [
        'b_title' => "Unnamed Board",
        'b_def_authorname' => "No name",
        'b_closed' => false,
        'b_note' => ""
    ];

    protected $casts = [
        'b_closed' => 'boolean',
    ];

    public function scopeOfName($query, $name)
    {
        return $query->where('b_name', $name);
    }

    public function threads()
    {
        return $this->hasMany('App\Thread', 't_kjb', $this->primaryKey);
    }

}
