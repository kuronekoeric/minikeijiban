<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\AnonymousResourceCollection ;
use App\Http\Resources\Thread as ThreadResources;

class ThreadCollection extends AnonymousResourceCollection
{
    protected $rangeStr;

    function __construct($resource, $collects, $rangeStr)
    {
        parent::__construct($resource, $collects);
        $this->resource = $resource;
        $this->collects = $collects;
        $this->rangeStr = $rangeStr;
    }

    public function toArray($request)
    {
        return $this->collection->map(function (ThreadResources $resource) use ($request) {
            //$rangeStr
            return $resource->setRange($this->rangeStr)->toArray($request);
        })->all();
    }
}
