<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\Thread as ThreadResources;

class Board extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if(isset($this->b_id)){
            return [
                'id' => $this->b_id,
                'name' => $this->b_name,
                'title' => $this->b_title,
                'def_authorname' => $this->b_def_authorname,
                'threads' => ThreadResources::collection($this->threads()->visible()->orderBy("t_order", 'desc')->get(), '1-5')
                //'updated_at' => $this->updated_at,
            ];
        } else {
            abort(404);
        }
    }
}
