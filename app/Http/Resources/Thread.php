<?php

namespace App\Http\Resources;
use App\Http\Resources\Comment as CommentResources;

use Illuminate\Http\Resources\Json\JsonResource;

class Thread extends JsonResource
{

    protected $rangeStr = null;
    public function __construct($resource, $rangeStr = "last")
    {
        parent::__construct($resource);
        $this->resource = $resource;
        $this->rangeStr = $rangeStr;
    }

    public function setRange($rangeStr){
        $this->rangeStr = $rangeStr;
        return $this;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->t_id,
            'title' => $this->t_title,
            'order' => $this->t_order,
            'locked' => $this->t_locked,
            'idList' => $this->idList(),
            'comments' => CommentResources::collection($this->rangeOfComments($this->rangeStr)->get())
        ];
    }

    public static function collection($resource, $rangeStr = null){
        return new ThreadCollection($resource, get_called_class(), $rangeStr);
    }
}
