<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Comment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->m_id,
            'kid' => $this->m_kid,
            'author' => $this->m_author,
            'author_id' => $this->m_author_id,
            'mail' => $this->m_mail,
            'trip' => $this->m_trip,
            'content' => $this->m_invisible ? "The comment is invisible." : $this->m_content,
            'date' => $this->formatDate($this->m_date)
            //'thread' => $this->m_content,
        ];

        return parent::toArray($request);
    }

    private function formatDate($date){
        $ws = __('board.weeks');
        $ymd = $date->format('Y/m/d');
        $w = "(".$ws[$date->format('w')].")";
        $his = $date->format('H:i:s');
        return $ymd.$w." ".$his;

    }
}
