<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Board;
//use App\Http\Resources\Board as BoardResources;
use App\Thread;
use App\Comment;
use App\Http\Resources\Comment as CommentResources;

class BoardController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request){
        //
    }

    public function index($boardname){
        if ($boardname != "") {
            $boarddata = $this->getBoardData($boardname);
            if ($boarddata != null) {
                return view('board', ["board" => $boarddata]);
            } else {
                abort(404);
            }
        }
    }

    public function thread($boardname, $threadId, $range = "all"){
        if ($boardname != "") {
            $boarddata = $this->getBoardData($boardname);
            if ($boarddata != null) {
                $threaddatas = DB::table('mkb_thread')
                    ->where("t_kjb", "=", $boarddata->b_id)
                    ->where("t_id", "=", $threadId)
                    ->get();
                if (count($threaddatas) > 0) {
                    return view('thread', ["board" => $boarddata, "thread"=> $threaddatas[0], "range" => $range]);
                } else {
                    abort(404);
                }
            } else {
                abort(404);
            }
        }
    }

    public function insert($boardname, $tid = null, Request $request){
        if ($boardname != "") {
            $boarddata = $this->getBoardData($boardname);
            if ($boarddata != null) {
                $thread = null;
                $newThread = false;
                if(isset($request->t_title)){
                    $newThread = true;
                    $thread = new Thread();
                    $thread->t_title = $request->t_title;
                    $thread->t_order = time();
                    $thread->t_kjb = $boarddata->b_id;
                    $thread->save();
                } else if(isset($tid) || $tid == null){
                    $thread = Thread::find($tid);
                } else {
                    abort(500, "No tid");
                    return;
                }
                if($thread == null || empty($thread)){
                    abort(500, "Thread not found");
                    return;
                }
                $comment = new Comment();
                $comment->m_kjb = $boarddata->b_id;
                $comment->m_kid = $this->getNewKid($boarddata->b_id);
                $comment->m_author = !isset($request->m_author) || empty($request->m_author) ? $boarddata->b_def_authorname : $request->m_author;
                $comment->m_author_id = $this->_makeId($request);
                $comment->m_mail = isset($request->m_mail) ? $request->m_mail : "";
                $trip = $this->_makeTrip($request->m_trip);
                $comment->m_trip = $trip ? $trip : "";
                $comment->m_content = $request->m_content;
                //$comment->m_pass = $request->m_pass;
                $comment->m_pass = "";
                $comment->m_date = date("Y-n-d H:i:s");
                $comment->m_thread = $thread->t_id;
                $comment->save();
                //上げ、下げ機能
                if(!$newThread && $comment->m_mail != "sage"){
                    $thread->t_order = time();
                    $thread->save();
                }
            } else {
                abort(404);
            }
        }
    }

    function _makeId($request){
        $ip = $request->ip();
        $date = date("Y-m-d");
        $salt = "minikeiji";
        $chars = str_split("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/");
        $md5 = md5($ip . $date . $salt);
        $res = "";
        $strarr = str_split(substr($md5, 0, 12),3);
        foreach($strarr as $unit){
            $dec = hexdec($unit);
            $res .= $chars[floor($dec/64)] . $chars[$dec%64];
        }
        return $res;
    }

    /*-----------------------------------------------------
	 * make 2ch TRIP ( = DES ) by CASEY.JP 2010 GPL
	 *-----------------------------------------------------
	 * VERSION:
	 *   2010.08.17 0.01 alpha
	 * NOTE:
	 *   digit  8: -2002.10.03
	 *   digit 10: 2002.10.03-
	 *   digit 12: 2009.06.19-
	 * test data for digit 10:
	 *   $key = "#テスト";				// ◆SQ2Wyjdi7M
	 *   $key = '#Wikipedia';			// ◆Ig9vRBfuyA
	 *   $key = '#istrip';				// ◆/WG5qp963c
	 * test data for digit 12:
	 *   $key = '#TESTTESTTESTTEST';	// ◆TDxLX5/h3FIK
	 *   $key = '#テストテストテスト';	// ◆Iwo2kAiiMPp6
	 * SJIS CODE in SCRIPT
	 */
    function _makeTrip($key){
		// check
		preg_match('|^#(.*)$|', $key, $keys);
		if(empty($keys[1])) return false;
		$key = $keys[1];

		// start
		if(strlen($key) >= 12){
			 // digit 12
			$mark = substr($key, 0, 1);
			if($mark == '#' || $mark == '$'){
				if(preg_match('|^#([[:xdigit:]]{16})([./0-9A-Za-z]{0,2})$|', $key, $str)){
					$trip = substr(crypt(pack('H*', $str[1]), "$str[2].."), -10);
				}else{
					// ext
					$trip = '???';
				}
			}else{
				$trip = substr(base64_encode(sha1($key, TRUE)), 0, 12);
				$trip = str_replace('+', '.', $trip);
			}
		}else{
			// digit 10
			$tripkey = htmlspecialchars($key, ENT_QUOTES);
			$salt = htmlspecialchars($key, ENT_QUOTES);
			$salt = substr($tripkey . 'H.', 1, 2);

			$pattern = '/[\x00-\x20\x7B-\xFF]/';
			$salt = preg_replace($pattern, ".", $salt);

			$patterns = ":;<=>?@[\\]^_`";
			$mach = "ABCDEFGabcdef";

			for($i=0; $i<=13-1; $i++){
				$salt = str_replace($patterns[$i], $mach[$i], $salt);
			}

			$trip = crypt($tripkey, $salt);
			$trip = substr($trip, -10);
		}
		$trip = '◆'.$trip;
		return $trip;
	}

    function getNewKid($boardId) {
        $comment = Comment::ofBoard($boardId)->orderBy("m_kid","DESC")->take(1)->first();
        if(!$comment){
            return 1;
        } else {
            return $comment->m_kid + 1;
        }

    }

    private function getBoardData($boardname){
        $board = Board::where('b_name', $boardname)->first();
        return $board;
    }
}
