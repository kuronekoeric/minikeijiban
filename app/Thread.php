<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    protected $table = 'mkb_thread';
    protected $primaryKey = 't_id';

    //protected $guarded = ['b_closed'];

    //public $timestamps = false;

    const CREATED_AT = 't_creation_date';
    const UPDATED_AT = 't_last_update';

    const DEF_RANGE = [50, 'n'];
    protected $idList = null;

    protected $attributes = [
        't_title' => "No title",
        't_order' => 0,
        't_invisible' => false,
        't_locked' => false
    ];

    protected $casts = [
        'b_closed' => 'boolean',
        't_locked' => 'boolean'
    ];

    public function board()
    {
        return $this->belongsTo('App\Board', 't_kjb', 'b_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment', 'm_thread', $this->primaryKey);
    }

    public function idList(){
        if($this->idList == null){
            $rows = $this->comments()->get("m_kid");
            $results = [];
            foreach ($rows as $row) {
                $results[] = $row->m_kid;
            }
            $this->idList = $results;
        }
        return $this->idList;
    }

    public function rangeOfComments($rangeStr){
        $rangeParams = $this->handleRange($rangeStr);
        return $this->comments()->range($rangeParams[0], $rangeParams[1]);
    }

    private function handleRange($rangeStr){
        $idList = $this->idList();
        $n = count($idList);
        if ($rangeStr == "all") {
            return [null, null];
        }
        if ($rangeStr == "last") {
            return [50, $n > 50 ? $n-50 : 0];
        }
        $patt = "/^([0-9]+|[0-9]+-[0-9]+)$/";
        if (!preg_match($patt, $rangeStr)) {
            return self::DEF_RANGE;
        }
        $rangeArr = explode("-", $rangeStr);
        $n = count($rangeArr);
        if ($n == 1) {
            return [1, intval($rangeArr[0]) - 1];
        } else if ($n == 2) {
            $from = intval($rangeArr[0]);
            $to = intval($rangeArr[1]);
            if ($to >= $from) {
                return [$to - $from + 1, $from - 1];
            } else {
                return self::DEF_RANGE;
            }
        }
    }



    public function scopeOfBoard($query, $boardId)
    {
        return $query->where('t_kjb', $boardId);
    }

    public function scopeOfBoardThread($query, $boardId, $threadId)
    {
        return $query->where('t_kjb', $boardId)->where($this->primaryKey, $threadId);
    }

    public function scopeVisible($query){
        return $query->where('t_invisible', false);
    }

}
