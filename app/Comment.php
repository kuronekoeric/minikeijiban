<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    protected $table = 'mkb_comment';
    protected $primaryKey = 'm_id';

    const CREATED_AT = 'm_creation_date';
    const UPDATED_AT = 'm_last_update';

    protected $attributes = [
        'm_author' => "No name",
        'm_mail' => "",
        'm_trip' => "",
        'm_content' => "",
        'm_pass' => "",
        'm_invisible' => false
    ];

    protected $casts = [
        'm_invisible' => 'boolean',
        'm_date' => 'datetime'
    ];

    public function scopeOfThread($query, $threadId)
    {
        return $query->where('m_thread', $threadId);
    }

    public function scopeOfBoard($query, $boardId)
    {
        return $query->where('m_kjb', $boardId);
    }
    public function thread()
    {
        return $this->belongsTo('App\Thread', 'm_thread', 't_id');
    }

    public function scopeRange($query, $limit = 5, $offset = null)
    {
        if ($offset != null) {
            $query->offset($offset);
        }
        if ($limit != null) {
            $query->limit($limit);
        }
        return $query;
    }

    public function scopeOfThreadKid($query, $threadId, $kid)
    {
        return $query->where('m_thread', $threadId)->where('m_kid', $kid);
    }
}
