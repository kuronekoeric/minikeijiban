<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="api-base-url" content="{{ url('api/board/'.$board->b_name) }}" />
    <meta name="base-url" content="{{ url('board/'. $board->b_name) }}" />
    <meta name="app-lang" content="{{ Lang::locale() }}" />
    <title>mini board - {{ $board->b_title }}</title>
    <link href="{{url(mix('css/app.css'))}}" rel="stylesheet" type="text/css">
</head>
<body>
    <div id="root"></div>
    <script src="{{url(mix('js/manifest.js'))}}"></script>
    <script src="{{url(mix('js/vendor.js'))}}"></script>
    <script src="{{url(mix('js/board.js'))}}"></script>
</body>
</html>
