const ja_JP = {
    'thr.read.all': '全部読む',
    'thr.read.last': '最新50',
    'thr.read.front': '1-100',
    'thr.read.refresh': 'リロード',
    'thr.read.p100': '前100',
    'thr.read.n100': '次100',
    'thr.back': '掲示板に戻る',
    'cm.form.author': 'お名前',
    'cm.form.trip': 'トリップ',
    'cm.form.mail': 'メールアドレス',
    'cm.form.title': 'スレッドタイトル',
    'cm.form.content': 'コメント內容',
    'cm.form.submit': '書き込む',

};
export default ja_JP;
