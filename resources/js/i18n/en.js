const en_US = {
    'thr.read.all': 'Read all',
    'thr.read.last': 'The least 50',
    'thr.read.front': '1-100',
    'thr.read.refresh': 'Refresh',
    'thr.read.p100': 'Prev 100',
    'thr.read.n100': 'Next 100',
    'thr.back': 'Back to board',
    'cm.form.author': 'Name',
    'cm.form.trip': 'Trip',
    'cm.form.mail': 'E-mail',
    'cm.form.title': 'Title',
    'cm.form.content': 'Content',
    'cm.form.submit': 'Post',
};
export default en_US;
