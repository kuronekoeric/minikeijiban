const zh_TW = {
    'thr.read.all': '閱讀全部',
    'thr.read.last': '最新50',
    'thr.read.front': '1-100',
    'thr.read.refresh': '重新整理',
    'thr.read.p100': '前100',
    'thr.read.n100': '後100',
    'thr.back': '回到版面',
    'cm.form.author': '名稱',
    'cm.form.trip': 'Trip',
    'cm.form.mail': 'E-mail',
    'cm.form.title': '標題',
    'cm.form.content': '內容',
    'cm.form.submit': '投稿',
};
export default zh_TW;
