import React from 'react'
import { useSelector } from 'react-redux'
import axios from 'axios';
import { Chip, ListItem, ListItemText, Typography, Popover, CircularProgress } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
//import { mdiChevronDoubleRight } from '@mdi/js';
//import Icon from '@mdi/react'
import ChevronDoubleRight from 'mdi-material-ui/ChevronDoubleRight'

const useStyles = makeStyles(theme => ({
    card: {
        maxWidth: 800,
        marginTop: theme.spacing(2)
    },
    reftag:{
        height: "auto"
    },
    reftagDiv:{
        display: "inline-block"
    },
    commentTitle:{
        marginRight: theme.spacing(1)
    },
    authorName:{
        color: "green"
    },
    authorTrip:{
        marginRight: theme.spacing(1),
    },
    subinfo:{
        marginRight: theme.spacing(1),
        color: "gray"
    }
}));

function RefTag(props) {
    const classes = useStyles();
    const cidData = useSelector(state => state.boardReducer.threads_cid_data);
    const cidList = (cidData != null &&　"t" + props.tid in cidData) ? cidData["t" + props.tid]:[];
    const isValid = cidList.indexOf(props.num) != -1 && props.num != props.kid;
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [refContent, setRefContent] = React.useState(null);
    const handlePopoverOpen = event => {
        setAnchorEl(event.currentTarget);
        if(refContent === null){
            axios.get("/comment/" + props.tid + "-" + props.num)
            .then(res => {
                //console.log(res);
                let data = res.data.data;
                let comment = <CommentContent tid={props.tid} kid={data.kid} author={data.author} content={data.content} date={data.date} authorId={data.author_id} />
                setRefContent(comment);
            })
        }
    };
    const handlePopoverClose = () => {
        setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    return (
        <div className={classes.reftagDiv}>
            <Chip
                className={classes.reftag}
                icon={<ChevronDoubleRight />}
                label={props.num}
                disabled={!isValid}
                onClick={handlePopoverOpen}
            />
            <Popover
                id={"popover-" + props.num}
                open={open}
                anchorEl={anchorEl}
                anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
                }}
                transformOrigin={{
                vertical: 'top',
                horizontal: 'left',
                }}
                onClose={handlePopoverClose}
                disableRestoreFocus
            >
                <div>{refContent == null ? <CircularProgress /> : refContent}</div>
            </Popover>
        </div>
    );
}

function RefTagChanger(props) {
    const refRegex = />>[0-9]+/g;
    const refs = props.text.match(refRegex);
    //refs = refRegex.exec(item);
    const content = refs != null ? props.text.split(refRegex).map((item, key) => {
        return <React.Fragment key={key}>
            {refs[key] != null ? (<RefTag num={parseInt(refs[key].substr(2))} tid={props.tid} kid={props.kid} />) : ("")}
            {item}
        </React.Fragment>
    }) : props.text;
    return (
        <React.Fragment>
            {content}
        </React.Fragment>
    );
}


function FormatContent(props) {
    const content = props.text.split('\n').map((item, key) => {
        return <React.Fragment key={key}><RefTagChanger text={item} tid={props.tid} kid={props.kid} /><br /></React.Fragment>
    });
    return (
        <div>
            {content}
        </div>
    );
}

export default function CommentContent(props) {
    const classes = useStyles();
    return (
        <ListItem className={classes.card}>
            <ListItemText
                disableTypography
                primary={
                    <strong>
                        <span className={classes.commentTitle}>{props.kid}</span>
                        <span className={classes.authorName}>{props.author}</span>
                        <span className={classes.authorTrip}>{props.trip}</span>
                        <span className={classes.subinfo}>{props.date}</span>
                        <span className={classes.subinfo}>ID:{props.authorId}</span>
                    </strong>
                }
                secondary={
                    <React.Fragment>
                        <Typography
                            component="span"
                            variant="body2"
                            className={classes.inline}
                            color="textPrimary"
                        >
                            <FormatContent text={props.content} tid={props.tid} kid={props.kid} />
                        </Typography>
                    </React.Fragment>
                }
            />
        </ListItem>
    )
}
