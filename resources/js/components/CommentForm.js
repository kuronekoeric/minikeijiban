import React, { useState } from 'react'
import {Button, CssBaseline, Grid, Paper, Snackbar, TextField, makeStyles} from '@material-ui/core/';
import axios from 'axios';
import { FormattedMessage } from 'react-intl';
import MySnackbarContentWrapper from './MySnackbarContentWrapper';

const useStyles = makeStyles(theme => ({
    paper: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: theme.spacing(3)
    },
    form: {
        width: '100%', // Fix IE 11 issue.
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function CommentForm({ tid = null, onReload = () => { } }) {
    const classes = useStyles();
    const initFormdata = {
        m_author: "",
        m_mail: "",
        m_content: ""
    };
    const [formData, setFormData] = useState(initFormdata);
    const [snackbarState, setSnackbarState] = useState({
        open: false,
        message: '',
        variant: 'info',
    });
    const handleSubmit = (e) => {
        console.log(formData)
        e.preventDefault();
        if (formData.m_content != "" && formData.m_content.length > 5) {
            axios.post("/write" + (tid == null ? "" : "/" + tid), formData)
                .then(res => {
                    console.log(res);
                    if (res.status == 200) {
                        onReload();
                        setFormData(initFormdata);
                        setSnackbarState({
                            open: true,
                            message: 'Success to save the comment.',
                            variant: 'success'
                        });
                    } else {
                        setSnackbarState({
                            open: true,
                            message: 'Failed to save the comment.',
                            variant: 'error'
                        });
                    }
                    /*setboardData(res.data.data);
                    dispatch({
                        type: 'UPDATE',
                        data: getIdsData([res.data.data])
                    });*/
                });
        }
    }
    const handleChange = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        formData[nam] = val;
        setFormData({ ...formData });
    }
    const handleSnackbarClose = () => {
        setSnackbarState({ ...snackbarState, open: false });
    }
    return (<div className="commentForm">
        <Paper className={classes.paper}>
            <form className={classes.form} noValidate onSubmit={handleSubmit}>
                <Grid container spacing={2}>
                    <CssBaseline />
                    <Grid item xs={8}>
                        <TextField
                            variant="outlined"
                            required
                            fullWidth
                            id="author"
                            label={<FormattedMessage id="cm.form.author" />}
                            name="m_author"
                            onChange={handleChange}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <TextField
                            variant="outlined"
                            fullWidth
                            id="trip"
                            label={<FormattedMessage id="cm.form.trip" />}
                            name="m_trip"
                            onChange={handleChange}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            variant="outlined"
                            required
                            fullWidth
                            id="mail"
                            label={<FormattedMessage id="cm.form.mail" />}
                            name="m_mail"
                            onChange={handleChange}
                        />
                    </Grid>
                    {tid === null ? (
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                id="title"
                                label={<FormattedMessage id="cm.form.title" />}
                                name="t_title"
                                onChange={handleChange}
                            />
                        </Grid>
                    ) : ("")}
                    <Grid item xs={12}>
                        <TextField
                            variant="outlined"
                            required
                            fullWidth
                            multiline
                            rows="5"
                            id="content"
                            name="m_content"
                            onChange={handleChange}
                        />
                    </Grid>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        {<FormattedMessage id="cm.form.submit" />}
                    </Button>
                </Grid>
            </form>
            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                open={snackbarState.open}
                onClose={handleSnackbarClose}
                autoHideDuration={1000}
                message={snackbarState.message}
            >
                <MySnackbarContentWrapper
                    onClose={handleSnackbarClose}
                    variant={snackbarState.variant}
                    message={snackbarState.message}
                />
            </Snackbar>
        </Paper>
    </div>);

}
