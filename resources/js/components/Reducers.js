const initState = {
    threads_cid_data: {}
};

export default function boardReducer(state = initState, action) {
    switch (action.type) {
        case 'BOARD_UPDATE':
            return { threads_cid_data: action.data };
        default:
            return state;
    }
}
