import React from 'react'
import CommentContent from './CommentContent'
import { useSelector } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles';
import { Button, Card, CardActions, CardContent, Divider, List, Typography } from '@material-ui/core';
import { FormattedMessage } from 'react-intl';

const baseUrl = document.head.querySelector('meta[name="base-url"]').content;
const useStyles = makeStyles(theme => ({
    paper: {
        //display: 'flex',
        padding: theme.spacing(3),
        marginTop: theme.spacing(2)
    },
    list: {
        width: '100%'
    }
}));

export default function ThreadContent(props) {
    const classes = useStyles();
    const cidData = useSelector(state => state.boardReducer.threads_cid_data);
    const cidList = (cidData != null && "t" + props.tid in cidData) ? cidData["t" + props.tid] : [];
    const commentList = props.comments.map((row, i) =>
        <React.Fragment key={row.id}>
            <CommentContent tid={props.tid} kid={row.kid} author={row.author} content={row.content} date={row.date} authorId={row.author_id} trip={row.trip}  />
            {i < props.comments.length - 1 ? (<Divider component="li" />) : ("")}
        </React.Fragment>);
    const handleThreadLink = (range) => {
        return () => {
            location.href = baseUrl + "/thread/" + props.tid + "/" + range
        }
    }
    return (
        <Card className={classes.paper}>
            <CardContent>
                <Typography variant="h5" component="h3">
                    {props.title + " " + "(" + cidList.length + ")"}
                </Typography>
                <List className={classes.list}>
                    {commentList}
                </List>
            </CardContent>
            <CardActions>
                <Button size="small" color="primary" onClick={handleThreadLink("all")}>
                    <FormattedMessage id="thr.read.all" />
                </Button>
                <Button size="small" color="primary" onClick={handleThreadLink("last")}>
                    <FormattedMessage id="thr.read.last" />
                </Button>
                <Button size="small" color="primary" onClick={handleThreadLink("1-100")}>
                    <FormattedMessage id="thr.read.front" />
                </Button>
                <Button size="small" color="primary">
                    <FormattedMessage id="thr.read.refresh" />
                </Button>
            </CardActions>
        </Card>
    );

}
