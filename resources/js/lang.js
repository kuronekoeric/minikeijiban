import React, { useState } from "react";
import { IntlProvider } from "react-intl";
import { useDispatch, useSelector } from 'react-redux'
import en_US from './i18n/en.js';
import ja_JP from './i18n/ja.js';
import zh_TW from './i18n/zh.js';

if (!Intl.RelativeTimeFormat) {
    //require('@formatjs/intl-relativetimeformat/polyfill');
    require('@formatjs/intl-relativetimeformat/dist/locale-data/en');
    require('@formatjs/intl-relativetimeformat/dist/locale-data/ja');
    require('@formatjs/intl-relativetimeformat/dist/locale-data/zh');
  }

const appURL = new URL(location.href);
const defLang = document.head.querySelector('meta[name="app-lang"]').content;
const initLang = appURL.searchParams.has("lang")? appURL.searchParams.get("lang") : defLang;


export function langReducer(state = initLang, action) {
    switch (action.type) {
        case 'CHANGE_LANG':
            return action.lang;
        default:
            return state;
    }
}

export function setLang(lang){
    const dispatch = useDispatch();
    dispatch({
        type: 'CHANGE_LANG',
        lang: lang
    });
}

export function MyIntlProvider(props){
    //const [locale, setLocale] = useState(initLang);
    const locale = useSelector(state => state.langReducer);

    let messages;
    if (locale.includes('zh')) {
        messages = zh_TW;
      } else if (locale.includes('ja')) {
        messages = ja_JP;
      } else {
        messages = en_US;
      }
    return (<IntlProvider locale={locale} key={locale} defaultLocale="en" messages={messages}>
        {props.children}
        </IntlProvider>
        );
}
