require("./bootstrap");
import React, { useState } from "react";
import ReactDOM from "react-dom";
import { createStore, combineReducers } from 'redux'
import { useDispatch, Provider } from 'react-redux'
import CommentForm from "./components/CommentForm";
import { Button, ButtonGroup, Container, Divider, Link } from '@material-ui/core';
import axios from 'axios';
import CommentContent from './components/CommentContent'
import boardReducer from "./components/Reducers";
import { FormattedMessage } from 'react-intl';
import { MyIntlProvider, langReducer } from "./lang";

const threadId = document.head.querySelector('meta[name="thread-id"]').content;
const initRange = document.head.querySelector('meta[name="range"]').content;
const baseUrl = document.head.querySelector('meta[name="base-url"]').content;
const debug = true;

const initState = {
    title: "",
    order: -1,
    comments: []
};

const rootReducer = combineReducers({
    boardReducer,
    langReducer
})

var store;
if(debug){
    store = createStore(rootReducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
} else {
    store = createStore(rootReducer);
}

function getIdsData(data) {
    let result = {}, i = 0;
    for (i = 0; i < data.length; i++) {
        result["t" + data[i].id] = data[i].idList;
    }
    return result;
}

function App() {
    const [boardData, setboardData] = useState(initState);
    const [range, setRange] = useState(initRange);
    const dispatch = useDispatch();
    const loadData = (range) => {
        axios.get("/thread/" + threadId + "/" + range)
            .then(res => {
                setboardData(res.data.data);
                dispatch({
                    type: 'BOARD_UPDATE',
                    data: getIdsData([res.data.data])
                });
            })
    }
    if (boardData.order == -1) {
        loadData(range);
    }
    const changeRange = (nRange) => {
        if(nRange != range){
            setRange(nRange);
            loadData(nRange);
            window.history.pushState(null, null, baseUrl + "/thread/" + threadId + "/" + nRange);
        }
    }
    const commentList = boardData.comments.map((row, i) =>
        <React.Fragment key={row.id}>
            <CommentContent tid={threadId} kid={row.kid} author={row.author} content={row.content} date={row.date} authorId={row.author_id} trip={row.trip}/>
            {i < boardData.comments.length - 1 ? (<Divider component="hr" />) : ("")}
        </React.Fragment>);
    return (
        <div className="App">
            <Container component="main" maxWidth="md">
                <h1 className="mkb-head-title">{boardData.title}</h1>
                <Link href={baseUrl}><FormattedMessage id="thr.back" /></Link>
                <hr />
                <ButtonGroup
                    size="small"
                    aria-label="small outlined button group"
                >
                    <Button onClick={() => {changeRange("all"); }}><FormattedMessage id="thr.read.all" /></Button>
                    <Button onClick={() => {changeRange("last"); }}><FormattedMessage id="thr.read.last" /></Button>
                    <Button onClick={() => {changeRange("1-100"); }}><FormattedMessage id="thr.read.front" /></Button>
                </ButtonGroup>
                {commentList}
                <hr />
                <CommentForm tid={threadId}  onReload={()=>{loadData(range)}} />
            </Container>
        </div>
    )
}

if (document.getElementById("root")) {
    ReactDOM.render(
        <Provider store={store}>
            <MyIntlProvider>
                <App />
            </MyIntlProvider>
        </Provider>,
        document.getElementById("root"));
}
