require("./bootstrap");
import React, { useState } from "react";
import ReactDOM from "react-dom";
import { createStore, combineReducers } from 'redux'
import { useDispatch, Provider } from 'react-redux'
import CommentForm from "./components/CommentForm";
import Container from '@material-ui/core/Container';
import axios from 'axios';
import ThreadContent from "./components/ThreadContent";
import boardReducer from "./components/Reducers";
import { MyIntlProvider, langReducer } from "./lang";

const debug = true;

const initState = {
    title: "",
    threads: []
};

const rootReducer = combineReducers({
    boardReducer,
    langReducer
})

var store;
if (debug) {
    store = createStore(rootReducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
} else {
    store = createStore(rootReducer);
}

function getIdsData(data) {
    let result = {}, i = 0;
    for (i = 0; i < data.length; i++) {
        result["t" + data[i].id] = data[i].idList;
    }
    return result;
}

function App() {
    const [boardData, setboardData] = useState(initState);
    const dispatch = useDispatch();
    const loadData = () => {
        axios.get("")
        .then(res => {
            setboardData(res.data.data);
            dispatch({
                type: 'BOARD_UPDATE',
                data: getIdsData(res.data.data.threads)
            });
        })
    }
    if (boardData.name == null) {
        loadData();
    }
    //onReload
    const threadList = boardData.threads.map((row, key) =>
        <ThreadContent key={key} tid={row.id} title={row.title} comments={row.comments} />
    );
    return (
        <div className="App">
            <Container component="main" maxWidth="md">
                <h1 className="mkb-head-title">{boardData.title}</h1>
                <hr />
                <CommentForm onReload = {()=>{loadData()}} />
                {threadList}
            </Container>
        </div>
    )
}

if (document.getElementById("root")) {
    /*navigator.language*/
    //const [locale, setLocale] = useState(initLang);
    //console.log("render root");
    ReactDOM.render(
        <Provider store={store}>
            <MyIntlProvider>
                <App />
            </MyIntlProvider>
        </Provider>,
        document.getElementById("root")
    );
}
