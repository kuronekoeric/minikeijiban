<?php

use Illuminate\Http\Request;

use App\Board;
use App\Http\Resources\Board as BoardResources;
use App\Thread;
use App\Http\Resources\Thread as ThreadResources;
use App\Comment;
use App\Http\Resources\Comment as CommentResources;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('board/{name}', function ($boardname) {
    return new BoardResources(Board::ofName($boardname)->first());
});

//Route::get('board/{name}/data', 'BoardController@data');

function threadRoute($boardname, $tid, $rangeStr = "all") {
    $board = Board::ofName($boardname)->first();
    if (isset($board->b_id)) {
        return new ThreadResources(Thread::OfBoardThread($board->b_id, $tid)->first(), $rangeStr);
    } else {
        abort(404);
    }
}


Route::get('board/{name}/thread/{tid}', function ($boardname, $tid) {
    return threadRoute($boardname, $tid);
});

Route::get('board/{name}/thread/{tid}/{range}', function ($boardname, $tid, $rangeStr) {
    return threadRoute($boardname, $tid, $rangeStr);
});

Route::get('board/{name}/comment/{tid}-{kid}', function ($boardname, $tid, $kid) {
    $board = Board::ofName($boardname)->first();
    if (isset($board->b_id)) {
        return new CommentResources(Comment::OfThreadKid($tid, $kid)->first());
    } else {
        abort(404);
    }
});

Route::post('board/{name}/write/{tid?}', 'BoardController@insert');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
