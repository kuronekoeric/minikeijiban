<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/board/{name}', 'BoardController@index');

Route::get('/board/{name}/thread/{tid}', 'BoardController@thread');

Route::get('/board/{name}/thread/{tid}/{range}', 'BoardController@thread');

Route::get('/test', function () {
    return "test";
});

