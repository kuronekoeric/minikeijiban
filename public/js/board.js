(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["/js/board"],{

/***/ "./resources/js/board.js":
/*!*******************************!*\
  !*** ./resources/js/board.js ***!
  \*******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! redux */ "./node_modules/redux/es/redux.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _components_CommentForm__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/CommentForm */ "./resources/js/components/CommentForm.js");
/* harmony import */ var _material_ui_core_Container__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/core/Container */ "./node_modules/@material-ui/core/esm/Container/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _components_ThreadContent__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/ThreadContent */ "./resources/js/components/ThreadContent.js");
/* harmony import */ var _components_Reducers__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/Reducers */ "./resources/js/components/Reducers.js");
/* harmony import */ var _lang__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./lang */ "./resources/js/lang.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

__webpack_require__(/*! ./bootstrap */ "./resources/js/bootstrap.js");











var debug = true;
var initState = {
  title: "",
  threads: []
};
var rootReducer = Object(redux__WEBPACK_IMPORTED_MODULE_2__["combineReducers"])({
  boardReducer: _components_Reducers__WEBPACK_IMPORTED_MODULE_8__["default"],
  langReducer: _lang__WEBPACK_IMPORTED_MODULE_9__["langReducer"]
});
var store;

if (debug) {
  store = Object(redux__WEBPACK_IMPORTED_MODULE_2__["createStore"])(rootReducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
} else {
  store = Object(redux__WEBPACK_IMPORTED_MODULE_2__["createStore"])(rootReducer);
}

function getIdsData(data) {
  var result = {},
      i = 0;

  for (i = 0; i < data.length; i++) {
    result["t" + data[i].id] = data[i].idList;
  }

  return result;
}

function App() {
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(initState),
      _useState2 = _slicedToArray(_useState, 2),
      boardData = _useState2[0],
      setboardData = _useState2[1];

  var dispatch = Object(react_redux__WEBPACK_IMPORTED_MODULE_3__["useDispatch"])();

  var loadData = function loadData() {
    axios__WEBPACK_IMPORTED_MODULE_6___default.a.get("").then(function (res) {
      setboardData(res.data.data);
      dispatch({
        type: 'BOARD_UPDATE',
        data: getIdsData(res.data.data.threads)
      });
    });
  };

  if (boardData.name == null) {
    loadData();
  } //onReload


  var threadList = boardData.threads.map(function (row, key) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_ThreadContent__WEBPACK_IMPORTED_MODULE_7__["default"], {
      key: key,
      tid: row.id,
      title: row.title,
      comments: row.comments
    });
  });
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "App"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Container__WEBPACK_IMPORTED_MODULE_5__["default"], {
    component: "main",
    maxWidth: "md"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "mkb-head-title"
  }, boardData.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_CommentForm__WEBPACK_IMPORTED_MODULE_4__["default"], {
    onReload: function onReload() {
      loadData();
    }
  }), threadList));
}

if (document.getElementById("root")) {
  /*navigator.language*/
  //const [locale, setLocale] = useState(initLang);
  //console.log("render root");
  react_dom__WEBPACK_IMPORTED_MODULE_1___default.a.render(react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_redux__WEBPACK_IMPORTED_MODULE_3__["Provider"], {
    store: store
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_lang__WEBPACK_IMPORTED_MODULE_9__["MyIntlProvider"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(App, null))), document.getElementById("root"));
}

/***/ }),

/***/ "./resources/js/components/ThreadContent.js":
/*!**************************************************!*\
  !*** ./resources/js/components/ThreadContent.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ThreadContent; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _CommentContent__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CommentContent */ "./resources/js/components/CommentContent.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/core */ "./node_modules/@material-ui/core/esm/index.js");
/* harmony import */ var react_intl__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-intl */ "./node_modules/react-intl/lib/index.js");






var baseUrl = document.head.querySelector('meta[name="base-url"]').content;
var useStyles = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_3__["makeStyles"])(function (theme) {
  return {
    paper: {
      //display: 'flex',
      padding: theme.spacing(3),
      marginTop: theme.spacing(2)
    },
    list: {
      width: '100%'
    }
  };
});
function ThreadContent(props) {
  var classes = useStyles();
  var cidData = Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["useSelector"])(function (state) {
    return state.boardReducer.threads_cid_data;
  });
  var cidList = cidData != null && "t" + props.tid in cidData ? cidData["t" + props.tid] : [];
  var commentList = props.comments.map(function (row, i) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, {
      key: row.id
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_CommentContent__WEBPACK_IMPORTED_MODULE_1__["default"], {
      tid: props.tid,
      kid: row.kid,
      author: row.author,
      content: row.content,
      date: row.date,
      authorId: row.author_id,
      trip: row.trip
    }), i < props.comments.length - 1 ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_4__["Divider"], {
      component: "li"
    }) : "");
  });

  var handleThreadLink = function handleThreadLink(range) {
    return function () {
      location.href = baseUrl + "/thread/" + props.tid + "/" + range;
    };
  };

  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_4__["Card"], {
    className: classes.paper
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_4__["CardContent"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_4__["Typography"], {
    variant: "h5",
    component: "h3"
  }, props.title + " " + "(" + cidList.length + ")"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_4__["List"], {
    className: classes.list
  }, commentList)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_4__["CardActions"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_4__["Button"], {
    size: "small",
    color: "primary",
    onClick: handleThreadLink("all")
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_intl__WEBPACK_IMPORTED_MODULE_5__["FormattedMessage"], {
    id: "thr.read.all"
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_4__["Button"], {
    size: "small",
    color: "primary",
    onClick: handleThreadLink("last")
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_intl__WEBPACK_IMPORTED_MODULE_5__["FormattedMessage"], {
    id: "thr.read.last"
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_4__["Button"], {
    size: "small",
    color: "primary",
    onClick: handleThreadLink("1-100")
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_intl__WEBPACK_IMPORTED_MODULE_5__["FormattedMessage"], {
    id: "thr.read.front"
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_4__["Button"], {
    size: "small",
    color: "primary"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_intl__WEBPACK_IMPORTED_MODULE_5__["FormattedMessage"], {
    id: "thr.read.refresh"
  }))));
}

/***/ }),

/***/ 1:
/*!*************************************!*\
  !*** multi ./resources/js/board.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! d:\xampp\Laravel\minikeijiban\resources\js\board.js */"./resources/js/board.js");


/***/ })

},[[1,"/js/manifest","/js/vendor"]]]);