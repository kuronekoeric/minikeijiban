# Mini keiji Ban [ミニ掲示板]　(Use Laravel + React)
The example 5ch-liked board developed by Laravel and React.

## How to setup
### Require software
- [Composer](https://getcomposer.org/)
- [Note.js](https://nodejs.org/)

### Setup
- Intall Composer and Note.js

- Create .env file (See .env.example)

- Update PHP labrary
```console
composer update
```

- Update JS labrary (Note.js)
```console
 npm install
```
- Create database by migration class
```console
php artisan migrate
```

## Link

- [Laravel](https://laravel.com/)
- [React](https://reactjs.org)

## License

The Software is open-source software licensed under the [GPLv3 license](https://www.gnu.org/licenses/gpl-3.0.html).
