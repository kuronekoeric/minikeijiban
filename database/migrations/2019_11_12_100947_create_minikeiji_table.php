<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMinikeijiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mkb_keijiban', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            $table->increments('b_id');
            $table->string('b_name', 50)->unique();
            $table->string('b_title', 50);
            $table->string('b_def_authorname', 50);
            $table->boolean('b_closed');
            $table->string('b_note', 500);
            $table->timestamp('b_creation_date')->nullable();
            $table->timestamp('b_last_update')->nullable();
        });

        Schema::create('mkb_settings', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            $table->string('s_key', 50)->primary();
            $table->string('s_value', 200);
        });

        Schema::create('mkb_thread', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            $table->bigIncrements('t_id');
            $table->string('t_title', 200);
            $table->unsignedInteger('t_order');
            $table->boolean('t_invisible');
            $table->boolean('t_locked');
            $table->unsignedInteger('t_kjb');
            $table->timestamp('t_creation_date')->nullable();
            $table->timestamp('t_last_update')->nullable();
        });

        Schema::create('mkb_comment', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            $table->bigIncrements('m_id');
            $table->unsignedInteger('m_kjb');
            $table->unsignedBigInteger('m_kid');
            $table->string('m_author', 50);
            $table->string('m_author_id', 12);
            $table->string('m_mail', 50);
            $table->string('m_trip', 16);
            $table->string('m_content', 1024);
            $table->string('m_pass', 15);
            $table->dateTime('m_date');
            $table->unsignedBigInteger('m_thread');
            $table->boolean('m_invisible');
            $table->timestamp('m_creation_date')->nullable();
            $table->timestamp('m_last_update')->nullable();
        });

        Schema::table('mkb_comment', function (Blueprint $table) {
            $table->foreign('m_thread')
                ->references('t_id')
                ->on('mkb_thread')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->unique(["m_kjb", "m_kid"]);
        });

        Schema::table('mkb_thread', function (Blueprint $table) {
            $table->foreign('t_kjb')
                ->references('b_id')
                ->on('mkb_keijiban')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mkb_coment');
        Schema::dropIfExists('mkb_thread');
        Schema::dropIfExists('mkb_settings');
        Schema::dropIfExists('mkb_keijiban');
    }
}
